package Karts;

import java.util.ArrayList;

import org.newdawn.slick.SlickException;

import Main.Angle;

public abstract class AI extends Kart {

	public static final int STRAIGHT_ANGLE = 180;
	public static final int WAYPOINT_RADIUS = 250;

	private ArrayList<Float[]> waypointList = new ArrayList<Float[]>();
	private int index = 0;
	private Float[] target_waypoint;

	/**
	 * Creates a AI object
	 * 
	 * @throws SlickException
	 */
	public AI() throws SlickException {
		super();
	}

	/**
	 * Sets the target waypoint to the next waypoint in waypointList
	 */
	public void nextWaypoint() {
		if (index < waypointList.size()) {
			target_waypoint = waypointList.get(index++);
		}
	}

	/**
	 * Stores the list of waypoints and set the target waypoint to the first in
	 * the list
	 * 
	 * @param waypoints
	 *            Waypoints to follow
	 */
	public void setWaypoints(ArrayList<Float[]> waypoints) {
		this.waypointList = waypoints;
		this.target_waypoint = waypoints.get(0);
	}

	/**
	 * @return the target waypoint
	 */
	public Float[] getWaypoint() {
		return target_waypoint;
	}

	/**
	 * Calculates whether to turn right or left to reach the target angle
	 * 
	 * @param target_angle
	 *            The desired angle
	 * @param delta_angle
	 *            The increment that can be added or subtracted to reach target
	 *            angle
	 */
	protected void calcAngle(Angle target_angle, Angle delta_angle) {
		// Will turn left unless it is quicker to turn right to reach
		// target_angle
		if (getAngle().getDegrees() < target_angle.getDegrees()) {
			if (Math.abs(getAngle().getDegrees() - target_angle.getDegrees()) < STRAIGHT_ANGLE)
				setAngle(getAngle().add(delta_angle));
			else
				setAngle(getAngle().subtract(delta_angle));
		}
		// Will turn right unless it is quicker to turn left to reach
		// target_angle
		else {
			if (Math.abs(getAngle().getDegrees() - target_angle.getDegrees()) < STRAIGHT_ANGLE)
				setAngle(getAngle().subtract(delta_angle));
			else
				setAngle(getAngle().add(delta_angle));
		}
	}

	/**
	 * Move the kart towards the waypoint for a frame if possible
	 * 
	 * @param waypoint
	 *            x,y coordinates of the target waypoint
	 */
	protected abstract void move(Float[] waypoint);

	/**
	 * Update the AI position for a frame
	 */
	public void update() {
		float r = getWorld().calcDistanceTo(getX(), getY(), getWaypoint()[0], getWaypoint()[1]);
		// If within range of a waypoint get a new waypoint
		if (r < WAYPOINT_RADIUS) {
			nextWaypoint();
		}
		// Move and rotate
		move(getWaypoint());
	}
}
