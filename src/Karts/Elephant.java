package Karts;

import java.util.ArrayList;

import org.newdawn.slick.SlickException;

import Main.Angle;
import Main.Game;
import Main.World;

public class Elephant extends AI {

	private static final String ELEPHANT_IMAGE = Game.ASSETS_PATH + "/karts/elephant.png";

	private float nextX = 0;
	private float nextY = 0;

	/**
	 * Creates a Elephant AI
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @param waypoints
	 *            ArrayList of waypoints to follow
	 * @throws SlickException
	 */
	public Elephant(float x, float y, World world, ArrayList<Float[]> waypoints) throws SlickException {
		super();
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(ELEPHANT_IMAGE);
		this.setWaypoints(waypoints);
	}

	/**
	 * Move the kart towards the waypoint for a frame if possible
	 * 
	 * @param waypoint
	 *            x,y coordinates of the target waypoint
	 */
	protected void move(Float[] waypoint) {
		float mu = getWorld().getFriction(getX(), getY());
		Angle delta_angle = Angle.fromRadians(HEADING_CONSTANT);
		Angle target_angle = Angle.fromCartesian(waypoint[0] - getX(), waypoint[1] - getY());

		// If the kart has hit a hazard spin the kart
		if (hit_hazard()) {
			startSpinTimer();
			delta_angle = Angle.fromRadians(0.008);
			setAngle(getAngle().add(delta_angle));
		} else
			calcAngle(target_angle, delta_angle);

		// Find delta x and delta y
		setVelocity((getVelocity() + ACCELERATION) * (1 - mu));

		nextX = getX() + getAngle().getXComponent(getVelocity());
		nextY = getY() + getAngle().getYComponent(getVelocity());

		setRotation(getAngle().getDegrees());

		// If the next position doesn't cause a collision then move there
		if (!checkCollisions(nextX, nextY)) {
			setX(nextX);
			setY(nextY);
		}
	}
}
