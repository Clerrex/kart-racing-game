package Karts;

import java.util.ArrayList;

import org.newdawn.slick.SlickException;

import Main.Angle;
import Main.Game;
import Main.World;

public class Octopus extends AI {

	private static final String OCTOPUS_IMAGE = Game.ASSETS_PATH + "/karts/octopus.png";
	private static final int MIN_FOLLOW_DISTANCE = 100;
	private static final int MAX_FOLLOW_DISTANCE = 250;

	private Player player;
	private float nextX = 0;
	private float nextY = 0;

	/**
	 * Creates a Octopus AI
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @param waypoints
	 *            ArrayList of waypoints to follow
	 * @param player
	 *            The player on which octopus will follow
	 * @throws SlickException
	 */
	public Octopus(float x, float y, World world, ArrayList<Float[]> waypoints, Player player) throws SlickException {
		super();
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(OCTOPUS_IMAGE);
		this.player = player;
		this.setWaypoints(waypoints);
	}

	/**
	 * Move the kart towards the waypoint for a frame if possible
	 * 
	 * @param waypoint
	 *            x,y coordinates of the target waypoint
	 */
	protected void move(Float[] waypoint) {
		float mu = getWorld().getFriction(getX(), getY());
		Angle delta_angle = Angle.fromRadians(HEADING_CONSTANT);
		Angle target_angle = null;

		// If the kart has hit a hazard spin the kart
		if (hit_hazard()) {
			startSpinTimer();
			delta_angle = Angle.fromRadians(0.008);
			setAngle(getAngle().add(delta_angle));
		}
		// Handles whether to follow the player or not
		else {
			float r = getWorld().calcDistanceTo(getX(), getY(), player.getX(), player.getY());
			if (r > MIN_FOLLOW_DISTANCE && r < MAX_FOLLOW_DISTANCE) {
				target_angle = Angle.fromCartesian(player.getX() - getX(), player.getY() - getY());
			} else
				target_angle = Angle.fromCartesian(waypoint[0] - getX(), waypoint[1] - getY());

			calcAngle(target_angle, delta_angle);
		}

		setVelocity((getVelocity() + ACCELERATION) * (1 - mu));

		nextX = getX() + getAngle().getXComponent(getVelocity());
		nextY = getY() + getAngle().getYComponent(getVelocity());

		setRotation(getAngle().getDegrees());

		// If the next position doesn't cause a collision then move there
		if (!checkCollisions(nextX, nextY)) {
			setX(nextX);
			setY(nextY);
		}
	}
}
