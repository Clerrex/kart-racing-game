package Karts;

import java.util.ArrayList;

import org.newdawn.slick.SlickException;

import Main.Angle;
import Main.Game;
import Main.World;

public class Dog extends AI {

	private static final String DOG_IMAGE = Game.ASSETS_PATH + "/karts/dog.png";
	private static final float IN_FRONT_VELOCITY_MODIFIER = 0.9f;
	private static final float BEHIND_VELOCITY_MODIFIER = 1.1f;

	private float nextX = 0;
	private float nextY = 0;
	private Player player;

	/**
	 * Creates a Dog AI
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @param waypoints
	 *            ArrayList of waypoints to follow
	 * @param player
	 *            The player on which dog will adjust it's speed
	 * @throws SlickException
	 */
	public Dog(float x, float y, World world, ArrayList<Float[]> waypoints, Player player) throws SlickException {
		super();
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(DOG_IMAGE);
		this.setWaypoints(waypoints);
		this.player = player;
	}

	/**
	 * Move the kart towards the waypoint for a frame if possible
	 * 
	 * @param waypoint
	 *            x,y coordinates of the target waypoint
	 */
	protected void move(Float[] waypoint) {
		float mu = getWorld().getFriction(getX(), getY());
		Angle delta_angle = Angle.fromRadians(HEADING_CONSTANT);
		Angle target_angle = Angle.fromCartesian(waypoint[0] - getX(), waypoint[1] - getY());

		// If the kart has hit a hazard spin the kart
		if (hit_hazard()) {
			startSpinTimer();
			delta_angle = Angle.fromRadians(0.008);
			setAngle(getAngle().add(delta_angle));
		} else
			calcAngle(target_angle, delta_angle);

		// If in front of the player speed up
		if (player.getRank() > getRank())
			setVelocity((getVelocity() + IN_FRONT_VELOCITY_MODIFIER * ACCELERATION) * (1 - mu));
		// Else slow down
		else
			setVelocity((getVelocity() + BEHIND_VELOCITY_MODIFIER * ACCELERATION) * (1 - mu));

		nextX = getX() + getAngle().getXComponent(getVelocity());
		nextY = getY() + getAngle().getYComponent(getVelocity());

		setRotation(getAngle().getDegrees());

		// If the next position doesn't cause a collision then move there
		if (!checkCollisions(nextX, nextY)) {
			setX(nextX);
			setY(nextY);
		}
	}
}
