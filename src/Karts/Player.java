package Karts;

/* SWEN20003 Object Oriented Software Development
 * Kart Racing Game
 * Author: Michael Lumley <mlumley>
 */

import org.newdawn.slick.SlickException;

import Items.*;
import Main.Angle;
import Main.Game;
import Main.World;

public class Player extends Kart {

	private static final String PLAYER_IMAGE = Game.ASSETS_PATH + "/karts/donkey.png";

	private Item item = null;
	private float acceleration = ACCELERATION;
	private boolean item_in_use = false;

	/**
	 * Creates a Elephant AI
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @throws SlickException
	 */
	public Player(float x, float y, World world) throws SlickException {
		super();
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(PLAYER_IMAGE);
	}

	/**
	 * Get the item held by the player
	 * 
	 * @return item
	 */
	public Item getItem() {
		return item;
	}

	/**
	 * Set the item held by the player
	 * 
	 * @param item
	 */
	public void setItem(Item item) {
		this.item = item;
	}

	/**
	 * Get the kart acceleration
	 * 
	 * @return acceleration
	 */
	public double getAcceleration() {
		return acceleration;
	}

	/**
	 * Set the kart acceleration
	 * 
	 * @param acceleration
	 */
	public void setAcceleration(float boostAcceleration) {
		this.acceleration = boostAcceleration;
	}

	/**
	 * Set if an item is in use
	 * 
	 * @param item_in_use
	 */
	public void setItem_in_use(boolean item_in_use) {
		this.item_in_use = item_in_use;
	}

	/**
	 * Move the kart if posible
	 * 
	 * @param rotate_dir
	 *            The player's direction of rotation (-1 for anti-clockwise, 1
	 *            for clockwise, or 0).
	 * @param move_dir
	 *            The player's movement in the car's axis (-1, 0 or 1).
	 */
	private void move(double rotate_dir, double move_dir) {
		float heading, nextX, nextY;
		float mu = getWorld().getFriction(getX(), getY());
		Angle delta_angle = null;

		if (hit_hazard()) {
			startSpinTimer();
			delta_angle = Angle.fromRadians(0.008);
			move_dir = 1;
		} else {
			// Find angle that the player is facing
			heading = (float) (HEADING_CONSTANT * rotate_dir);
			delta_angle = Angle.fromRadians(heading);
		}

		setAngle(getAngle().add(delta_angle));
		setVelocity((float) ((getVelocity() + acceleration * move_dir) * (1 - mu)));
		// Find delta x and delta y
		nextX = getX() + getAngle().getXComponent(getVelocity());
		nextY = getY() + getAngle().getYComponent(getVelocity());

		setRotation(getAngle().getDegrees());
		if (!checkCollisions(nextX, nextY)) {
			// Collision with an item on the track
			getWorld().itemCollision(nextX, nextY);
			setX(nextX);
			setY(nextY);
		}
	}

	/**
	 * Update the game state for a frame.
	 * 
	 * @param rotate_dir
	 *            The player's direction of rotation (-1 for anti-clockwise, 1
	 *            for clockwise, or 0).
	 * @param move_dir
	 *            The player's movement in the car's axis (-1, 0 or 1).
	 * @param use_item
	 *            Time passed since last frame (milliseconds).
	 * @throws SlickException
	 */
	public void update(double rotate_dir, double move_dir, boolean use_item) throws SlickException {
		// Boost
		if ((use_item || item_in_use) && item != null) {
			// Needed so that the player we always move forwards when boosting
			move_dir = 1;
			item.use(this);
		}
		move(rotate_dir, move_dir);
	}
}
