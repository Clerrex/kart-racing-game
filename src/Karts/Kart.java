package Karts;

import org.newdawn.slick.SlickException;

import Main.Angle;
import Main.GameObject;

public abstract class Kart extends GameObject implements Comparable<Kart> {

	public static final float ACCELERATION = 0.0005f;
	public static final float HEADING_CONSTANT = 0.004f;
	private static int SPIN_TIMER = 700;

	private int rank;
	private float velocity = 0;
	private Angle angle = Angle.fromDegrees(0);
	private boolean hit_hazard = false;
	private int timer = SPIN_TIMER;

	/**
	 * Creates a AI object
	 * 
	 * @throws SlickException
	 */
	public Kart() throws SlickException {
		super();
	}

	/**
	 * @return rank of the kart
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * Set rank of kart
	 * 
	 * @param rank
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}

	/**
	 * @return velocity
	 */
	public float getVelocity() {
		return velocity;
	}

	/**
	 * Set Velocity of kart
	 * 
	 * @param velocity
	 */
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}

	/**
	 * @return angle
	 */
	public Angle getAngle() {
		return angle;
	}

	/**
	 * Set current angle of kart
	 * 
	 * @param angle
	 */
	public void setAngle(Angle angle) {
		this.angle = angle;
	}

	/**
	 * Whether the kart has hit a hazard
	 * 
	 * @return hit_hazard
	 */
	public boolean hit_hazard() {
		return hit_hazard;
	}

	/**
	 * Set whether the kart hit a hazard
	 * 
	 * @param hit_hazard
	 */
	public void setHit_hazard(boolean hit_hazard) {
		this.hit_hazard = hit_hazard;
	}

	/**
	 * Starts a timer for how long the kart should spin out when hiting a hazard
	 * Must be called every millisecond to work
	 */
	public void startSpinTimer() {
		if (timer >= 0) {
			setHit_hazard(true);
			timer--;
		} else {
			timer = SPIN_TIMER;
			setHit_hazard(false);
		}
	}

	/**
	 * Checks if a x, y coordinate will result in a collision
	 * 
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @return true or false
	 */
	public boolean checkCollisions(float x, float y) {
		// Check for the next tile for collision
		if (getWorld().isBlockedAt(x, y)) {
			setVelocity(0);
			return true;
		}

		// Collision with another kart
		if (getWorld().kartCollision(x, y)) {
			setVelocity(0);
			return true;
		}

		// Collision with a hazard on the track
		if (getWorld().hazardCollition(x, y)) {
			setHit_hazard(true);
			return true;
		}
		return false;
	}

	/**
	 * Used by Collections.sort to sort karts based on Y position
	 */
	public int compareTo(Kart other) {
		return (int) (this.getY() - other.getY());
	}

}
