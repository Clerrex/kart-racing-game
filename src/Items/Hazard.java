package Items;

import org.newdawn.slick.SlickException;

import Main.GameObject;

public abstract class Hazard extends GameObject {

	/**
	 * Creates a hazard object
	 * 
	 * @throws SlickException
	 */
	public Hazard() throws SlickException {
		super();
	}
}
