package Items;

import org.newdawn.slick.SlickException;

import Karts.Player;
import Main.GameObject;

public abstract class Item extends GameObject {

	public static final int SPAWN_DISTANCE = 41;

	/**
	 * Creates an Item object
	 * 
	 * @throws SlickException
	 */
	public Item() throws SlickException {
		super();
	}

	/**
	 * Transfers the item to the player and sets it inactive in the world
	 * 
	 * @param player
	 *            Player receiving the item
	 */
	public void pickup(Player player) {
		player.setItem(this);
		// So that the item can't be picked up again
		this.setActive(false);
	}

	/**
	 * Uses the item and removes it from the player
	 * 
	 * @param player
	 *            The player using the item
	 * 
	 * @throws SlickException
	 */
	public abstract void use(Player player) throws SlickException;
}
