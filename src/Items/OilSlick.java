/**
 * 
 */
package Items;

import org.newdawn.slick.SlickException;

import Main.World;

/**
 * @author Michael
 *
 */
public class OilSlick extends Hazard {

	private static final String OILSLICK_IMAGE = "assets/items/oilslick.png";

	/**
	 * Creates oil slick hazard
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @throws SlickException
	 */
	public OilSlick(float x, float y, World world) throws SlickException {
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(OILSLICK_IMAGE);
	}
}
