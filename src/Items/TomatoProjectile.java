package Items;

import org.newdawn.slick.SlickException;

import Karts.Player;
import Main.World;

public class TomatoProjectile extends Hazard {

	private static final String TOMATO_PROJECTILE_IMAGE = "assets/items/tomato-projectile.png";
	private static final float SPEED = 1.7f;
	private float dx;
	private float dy;

	/**
	 * Creates tomato projectile hazard
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @throws SlickException
	 */
	public TomatoProjectile(float x, float y, World world, Player player) throws SlickException {
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(TOMATO_PROJECTILE_IMAGE);
		dx = player.getAngle().getXComponent(SPEED);
		dy = player.getAngle().getYComponent(SPEED);
	}

	/**
	 * Moves the projectile at constant speed and is active
	 */
	public void update() {
		// If the projectile hits an underivable tile destroy it
		if (isActive() && getWorld().isBlockedAt(getX() + dx, getY() + dy)) {
			setActive(false);
		} else {
			setX(getX() + dx);
			setY(getY() + dy);
		}
	}
}
