package Items;

import org.newdawn.slick.SlickException;

import Karts.Player;
import Main.World;

public class Tomato extends Item {

	private static final String TOMATO_IMAGE = "assets/items/tomato.png";

	/**
	 * Creates tomato item
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @throws SlickException
	 */
	public Tomato(float x, float y, World world) throws SlickException {
		super();
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(TOMATO_IMAGE);
	}

	/**
	 * Uses the item and removes it from the player
	 * 
	 * @param player
	 *            The player using the item
	 * 
	 * @throws SlickException
	 */
	public void use(Player player) throws SlickException {
		float dx, dy;
		dx = (float) player.getAngle().getXComponent(SPAWN_DISTANCE);
		dy = (float) player.getAngle().getYComponent(SPAWN_DISTANCE);
		// Plus to spawn item in front of the player
		getWorld().addHazard(new TomatoProjectile(player.getX() + dx, player.getY() + dy, getWorld(), player));
		player.setItem(null);
	}
}
