package Items;

import org.newdawn.slick.SlickException;

import Karts.Player;
import Main.World;

public class Boost extends Item {

	private static final String BOOST_IMAGE = "assets/items/boost.png";
	private static final int BOOST_TIMER = 3000;
	private static final float BOOST_ACCELERATION = 0.0008f;
	private static final float NORMAL_ACCELERATION = 0.0005f;

	private int timer = BOOST_TIMER;

	/**
	 * Creates boost item
	 * 
	 * @param x
	 *            X coordinate to be created at
	 * @param y
	 *            Y coordinate to be created at
	 * @param world
	 *            Game world where it will exist
	 * @throws SlickException
	 */
	public Boost(float x, float y, World world) throws SlickException {
		super();
		this.setX(x);
		this.setY(y);
		this.setWorld(world);
		this.setImagePath(BOOST_IMAGE);
	}

	/**
	 * Use the item the item
	 * 
	 * @param player
	 *            The player using the item
	 */
	public void use(Player player) {
		if (timer >= 0) {
			player.setItem_in_use(true);
			player.setAcceleration(BOOST_ACCELERATION);
			timer--;
		} else {
			player.setItem_in_use(false);
			player.setAcceleration(NORMAL_ACCELERATION);
			player.setItem(null);
		}
	}
}
