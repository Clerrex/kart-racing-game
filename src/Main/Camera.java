package Main;

import Karts.Kart;

/* SWEN20003 Object Oriented Software Development
 * Kart Racing Game
 * Author: Michael Lumley <mlumley>
 */

public class Camera {

	private Kart kart;

	/**
	 * Creates a new camera object
	 * 
	 * @param kart
	 *            The kart to follow
	 */
	public Camera(Kart kart) {
		this.kart = kart;
	}

	/**
	 * Find the X position to start drawing the map from
	 * 
	 * @return X position
	 */
	public int getSX() {
		return (int) getCameraX(kart) / World.TILE_LENGTH;
	}

	/**
	 * Find the Y position to start drawing the map from
	 * 
	 * @return Y position
	 */
	public int getSY() {
		return (int) getCameraY(kart) / World.TILE_LENGTH;
	}

	/**
	 * Find the X offset to start drawing the map
	 * 
	 * @return X position
	 */
	public int getOffsetX() {
		// Negative to start drawing off screen
		return (int) -(getCameraX(kart) % World.TILE_LENGTH);
	}

	/**
	 * Find the Y offset to start drawing the map
	 * 
	 * @return Y position
	 */
	public int getOffsetY() {
		// Negative to start drawing off screen
		return (int) -(getCameraY(kart) % World.TILE_LENGTH);
	}

	/**
	 * Calculate the X position on the screen to draw GameObject
	 *
	 * @param kart
	 *            The GameObject which the camera will follow
	 */
	public double calcScreenX(GameObject obj) {
		return obj.getX() - getCameraX(kart);
	}

	/**
	 * Calculate the Y position on the screen to draw GameObject
	 *
	 * @param kart
	 *            The GameObject which the camera will follow
	 */
	public double calcScreenY(GameObject obj) {
		return obj.getY() - getCameraY(kart);
	}

	/**
	 * Returns the X position the camera should start drawing from
	 * 
	 * @param kart
	 *            The GameObject which the camera will follow
	 */
	public double getCameraX(GameObject obj) {
		return obj.getX() - Game.SCREENWIDTH / 2;
	}

	/**
	 * Returns the Y position the camera should start drawing from
	 * 
	 * @param kart
	 *            The GameObject which the camera will follow
	 */
	public double getCameraY(GameObject obj) {
		return obj.getY() - Game.SCREENHEIGHT / 2;
	}
}
