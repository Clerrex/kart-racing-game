package Main;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public abstract class GameObject {

	private float x;
	private float y;
	private Image image;
	private World world;
	private Float rotation = 0f;
	private Boolean active = true;

	/**
	 * Create a GameObject
	 * 
	 * @throws SlickException
	 */
	public GameObject() throws SlickException {

	}

	/**
	 * Get the X coordinate
	 * 
	 * @return X
	 */
	public float getX() {
		return x;
	}

	/**
	 * Set the X coordinate
	 * 
	 * @param X
	 */
	public void setX(float nextX) {
		this.x = nextX;
	}

	/**
	 * Get the Y coordinate
	 * 
	 * @return y
	 */
	public float getY() {
		return y;
	}

	/**
	 * Get the Y coordinate
	 * 
	 * @param Y
	 */
	public void setY(float nextY) {
		this.y = nextY;
	}

	/**
	 * Get the world the object is in
	 * 
	 * @return world
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Set the world the object is in
	 * 
	 * @param world
	 */
	public void setWorld(World world) {
		this.world = world;
	}

	/**
	 * Get the rotation of the object
	 * 
	 * @return rotation angle in degrees
	 */
	public float getRotation() {
		return rotation;
	}

	/**
	 * Set the rotation of the object
	 * 
	 * @param rotation
	 */
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	/**
	 * Get the image of the object
	 * 
	 * @return image
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * Set the image path of the object
	 * 
	 * @param imagePath
	 *            File path to the image
	 * @throws SlickException
	 */
	public void setImagePath(String imagePath) throws SlickException {
		image = new Image(imagePath);
	}

	/**
	 * Get whether the object is active
	 * 
	 * @return active
	 */
	public Boolean isActive() {
		return active;
	}

	/**
	 * Set whether the object is active
	 * 
	 * @param active
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * Render the entire screen, so it reflects the current game state.
	 * 
	 * @param g
	 *            The Slick graphics object, used for drawing.
	 */
	public void render(Graphics g) throws SlickException {
		if (active) {
			image.setRotation(rotation);
			image.drawCentered((float) world.camera.calcScreenX(this), (float) world.camera.calcScreenY(this));
		}
	}
}
