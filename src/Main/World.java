package Main;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

/* SWEN20003 Object Oriented Software Development
 * Kart Racing Game
 * Author: Michael Lumley <mlumley>
 */

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import Items.*;
import Karts.*;

/**
 * Represents the entire game world. (Designed to be instantiated just once for
 * the whole game).
 */

/**
 * @author Michael
 *
 */

public class World {

	private static final String ITEM_LOCATIONS = "data/items.txt";
	private static final String OIL = "Oil can";
	private static final String TOMATO = "Tomato";
	private static final String BOOST = "Boost";
	private static final String MAP_LOCATION = Game.ASSETS_PATH;
	private static final String MAP_IMAGE = Game.ASSETS_PATH + "/map.tmx";

	private TiledMap map = new TiledMap(MAP_IMAGE, MAP_LOCATION);
	private static final String WAYPOINT_LOCATIONS = "data/waypoints.txt";
	private static final int TILES_TO_DISPLAY_X = 24;
	private static final int TILES_TO_DISPLAY_Y = 18;
	public static final int TILE_LENGTH = 36;

	private static final float PLAYER_X = 1332f;
	private static final float PLAYER_Y = 13086f;
	private static final float DOG_X = 1404f;
	private static final float DOG_Y = 13086f;
	private static final float ELEPHANT_X = 1260f;
	private static final float ELEPHANT_Y = 13086f;
	private static final float OCTOPUS_X = 1476f;
	private static final float OCTOPUS_Y = 13086f;
	private static final int STARTING_RANK = 1;
	private static final float CRASH_DISTANCE = 40f;
	private static final int FINISH_LINE = 1026;
	private static final float CRASH_FRICTION = 1.0f;

	public Camera camera;
	private Player player;
	private Panel panel;
	private boolean gameOver = false;

	private ArrayList<Kart> karts = new ArrayList<Kart>();
	private ArrayList<Item> items = new ArrayList<Item>();
	private ArrayList<Float[]> waypoints = new ArrayList<Float[]>();
	private ArrayList<Hazard> hazards = new ArrayList<Hazard>();

	/**
	 * Create a new World object.
	 * 
	 * @throws SlickException
	 * @throws IOException
	 */
	public World() throws SlickException, IOException {
		readWaypoints(waypoints, WAYPOINT_LOCATIONS);
		createKarts(karts);
		createItems(items, ITEM_LOCATIONS);
		camera = new Camera(player);
		panel = new Panel();
	}

	/**
	 * Creates all karts in the game and adds them to an ArrayList
	 * 
	 * @param karts
	 *            ArrayList to contain the karts
	 * @throws SlickException
	 * @throws IOException
	 */
	private void createKarts(ArrayList<Kart> karts) throws SlickException, IOException {
		player = new Player(PLAYER_X, PLAYER_Y, this);
		// Adds the player to calculate rank by sorting in calcRank
		karts.add(player);
		karts.add(new Elephant(ELEPHANT_X, ELEPHANT_Y, this, waypoints));
		karts.add(new Dog(DOG_X, DOG_Y, this, waypoints, player));
		karts.add(new Octopus(OCTOPUS_X, OCTOPUS_Y, this, waypoints, player));
	}

	/**
	 * Renders all karts on screen
	 * 
	 * @param karts
	 *            ArrayList with all karts
	 * @param g
	 *            The Slick graphics object, used for drawing
	 * @throws SlickException
	 */
	private void renderKarts(ArrayList<Kart> karts, Graphics g) throws SlickException {
		for (Kart i : karts) {
			i.render(g);
		}
	}

	/**
	 * Creates all items at a x,y location read from a file and adds them to an
	 * ArrayList
	 * 
	 * @param items
	 *            Array to contain the items
	 * @param file
	 *            File location
	 * @throws SlickException
	 * @throws IOException
	 */
	private void createItems(ArrayList<Item> items, String file) throws SlickException, IOException {
		String line = null;
		String[] lineArray = null;
		BufferedReader br = new BufferedReader(new FileReader(file));

		while ((line = br.readLine()) != null) {
			lineArray = line.split(",");
			if (lineArray[0].equals(OIL)) {
				Oil item = new Oil(Float.parseFloat(lineArray[1]), Float.parseFloat(lineArray[2]), this);
				items.add(item);
			} else if (lineArray[0].equals(TOMATO)) {
				Tomato item = new Tomato(Float.parseFloat(lineArray[1]), Float.parseFloat(lineArray[2]), this);
				items.add(item);
			} else if (lineArray[0].equals(BOOST)) {
				Boost item = new Boost(Float.parseFloat(lineArray[1]), Float.parseFloat(lineArray[2]), this);
				items.add(item);
			} else
				System.out.println("Error unrecognized item type read");
		}
		br.close();
	}

	/**
	 * Renders all items in an ArrayList on screen
	 * 
	 * @param items
	 *            ArrayList containing items
	 * @param g
	 *            The Slick graphics object, used for drawing.
	 * @throws SlickException
	 */
	private void renderItems(ArrayList<Item> items, Graphics g) throws SlickException {
		for (Item i : items)
			i.render(g);
	}

	/**
	 * Reads waypoints from a file and adds then to an ArrayList
	 * 
	 * @param waypoints
	 *            List to put waypoints in
	 * @param file
	 *            File reading from
	 * @throws IOException
	 */
	private void readWaypoints(ArrayList<Float[]> waypoints, String file) throws IOException {
		String line = null;
		String[] lineArray = null;
		BufferedReader br = new BufferedReader(new FileReader(file));

		while ((line = br.readLine()) != null) {
			lineArray = line.split(",");
			// Set to 2 for x and y position
			Float[] coords = new Float[2];
			for (int i = 0; i < 2; i++)
				coords[i] = Float.parseFloat(lineArray[i]);
			waypoints.add(coords);
		}
		br.close();

	}

	/**
	 * Add a hazard to the hazard array
	 * 
	 * @param hazard
	 *            The hazard to be added
	 */
	public void addHazard(Hazard hazard) {
		hazards.add(hazard);
	}

	/**
	 * Renders all hazards in an ArrayList
	 * 
	 * @param hazards
	 *            ArrayList containing hazards
	 * @param g
	 *            The Slick graphics object, used for drawing.
	 * @throws SlickException
	 */
	public void renderHazards(ArrayList<Hazard> hazards, Graphics g) throws SlickException {
		for (Hazard i : hazards)
			i.render(g);
	}

	/**
	 * Ranks all karts based on Y position
	 * 
	 * @param karts
	 *            All karts in the game
	 * @param startingRank
	 *            Initial rank
	 */
	private void calcRank(ArrayList<Kart> Karts, int staringrank) {
		int rank = STARTING_RANK;
		Collections.sort(karts);
		for (Kart i : karts)
			i.setRank(rank++);
	}

	/**
	 * Get the player's rank
	 * 
	 * @return rank
	 */
	public int getPlayerRank() {
		return player.getRank();
	}

	/**
	 * Calculate the distance from a point to another point
	 * 
	 * @param x1
	 *            X coordinate of the first point
	 * @param y1
	 *            Y coordinate of the first point
	 * @param x2
	 *            X coordinate of the second point
	 * @param y2
	 *            Y coordinate of the second point
	 * @return
	 */
	public float calcDistanceTo(float x1, float y1, float x2, float y2) {
		return (float) Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
	}

	/**
	 * Checks if a x, y position would collide with a kart
	 * 
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @return True or False
	 */
	public boolean kartCollision(float x, float y) {
		// Collision with other karts
		for (Kart i : karts) {
			// Kart is within crash distance of another kart that is not itself
			double r = calcDistanceTo(x, y, i.getX(), i.getY());
			if (r < CRASH_DISTANCE && r > 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if a x, y position would collide with a item
	 * 
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @return True or False
	 */
	public void itemCollision(float x, float y) {
		// Collision with items
		for (Item i : items) {
			// Item is within crash distance of an item that is active
			double r = calcDistanceTo(x, y, i.getX(), i.getY());
			if (r < CRASH_DISTANCE && r > 1 && i.isActive()) {
				i.pickup(player);
			}
		}
	}

	/**
	 * Checks if a x, y position would collide with a hazard
	 * 
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @return True or False
	 */
	public boolean hazardCollition(float x, float y) {
		// Collision with hazards
		for (Hazard i : hazards) {
			// Item is within crash distance of an item that is active
			double r = calcDistanceTo(x, y, i.getX(), i.getY());
			if (r < CRASH_DISTANCE && r > 1 && i.isActive()) {
				i.setActive(false);
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the coefficient of friction from the map at a x, y coordinate
	 * 
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @return friction value
	 */
	public float getFriction(float x, float y) {
		// 0 for first and only layer
		int id = map.getTileId((int) x / TILE_LENGTH, (int) y / TILE_LENGTH, 0);
		float mu = Float.parseFloat(map.getTileProperty(id, "friction", null));
		return mu;
	}

	public boolean isBlockedAt(float x, float y) {
		return getFriction(x, y) >= CRASH_FRICTION;
	}

	/**
	 * Update the game state for a frame.
	 * 
	 * @param rotate_dir
	 *            The player's direction of rotation (-1 for anti-clockwise, 1
	 *            for clockwise, or 0).
	 * @param move_dir
	 *            The player's movement in the car's axis (-1, 0 or 1).
	 * @param use_item
	 *            Whether an item has been used.
	 */
	public void update(double rotate_dir, double move_dir, boolean use_item) throws SlickException {

		// Check if the player has pasted the finish line
		if (player.getY() < FINISH_LINE) {
			// The player cannot move the kart or use items
			rotate_dir = 0;
			move_dir = 0;
			use_item = false;
			gameOver = true;
		}

		if (!gameOver)
			calcRank(karts, STARTING_RANK);

		// Move any Projectiles
		for (Hazard h : hazards) {
			if (h instanceof TomatoProjectile) {
				((TomatoProjectile) h).update();
			}
		}

		// Move karts
		for (Kart i : karts) {
			if (i != player)
				((AI) i).update();
			else
				player.update(rotate_dir, move_dir, use_item);
		}
	}

	/**
	 * Render the entire screen, so it reflects the current game state.
	 * 
	 * @param g
	 *            The Slick graphics object, used for drawing.
	 * @throws SlickException
	 */
	public void render(Graphics g) throws SlickException {
		map.render(camera.getOffsetX(), camera.getOffsetY(), camera.getSX(), camera.getSY(), TILES_TO_DISPLAY_X,
				TILES_TO_DISPLAY_Y);
		renderKarts(karts, g);
		renderItems(items, g);
		renderHazards(hazards, g);
		panel.render(g, player.getRank(), player.getItem());
		if (gameOver)
			g.drawString("You came " + Panel.ordinal(getPlayerRank()) + "!", Game.SCREENWIDTH / 2 - 50,
					Game.SCREENHEIGHT / 2 - 100);
	}
}
