# Kart Racing Game #

### What is this repository for? ###

A 2D top down racing game similar to Mario Kart.
There are 3 other AI racers, each have their own tactics to win.
Collect items to use against them to win the race.

### How do I get set up? ###

Simply import the project into eclipse and run